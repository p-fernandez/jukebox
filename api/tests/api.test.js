import chai from 'chai';
import chaiHttp from 'chai-http';
import app from '../server';
import Record from '../models/record';

const should = chai.should();
chai.use(chaiHttp);

describe('API', () => {
  describe('/GET Api endpoint', () => {
    it('it must show API endpoint message', done => {
      chai.request(app)
        .get('/api')
        .end((err, res) => {
          should.not.exist(err);
          res.status.should.be.equal(200);
          res.body.should.be.a('object');
          res.body.message.should.be.equal('API endpoint!');
          done();
        });
    });
  });
});

describe('Records', () => {
  describe('/POST Records create', () => {
    it('it must create a new record', done => {
      const record = new Record({
        title: '0:12 Revolution In Just Listening',
        band: 'Coalesce',
        format: 'LP',
        year: 1999,
        url: 'http://bandcamp.com/coalesce',
        label: 'Relapse'
      });
      chai.request(app)
        .post('/api/records/create')
        .send(record)
        .end((err, res) => {
          should.not.exist(err);
          res.status.should.be.equal(200);
          res.type.should.equal('application/json');
          res.body.should.be.a('object');
          res.body.should.have.property('message').eql('Successfully created');
          res.body.record.should.have.property('year').eql(1999);
          res.body.record.should.have.property('label').eql('Relapse');
          res.body.record.should.have.property('format').eql('LP');
          res.body.record.should.have.property('url').eql('http://bandcamp.com/coalesce');
          res.body.record.should.have.property('band').eql('Coalesce');
          res.body.record.should.have.property('title').eql('0:12 Revolution In Just Listening');
          done();
        });
    });
  });

  describe('/GET Records', () => {
    it('it must get all records', done => {
      chai.request(app)
        .get('/api/records')
        .end((err, res) => {
          should.not.exist(err);
          res.status.should.be.equal(200);
          res.type.should.equal('application/json');
          res.body.should.be.a('array');
          res.body.length.should.be.above(0);
          done();
        });
    });
  });

  describe('/GET /records/:recordId', () => {
    it('it must get a record with the id set', done => {
      const record = new Record({
        title: 'Rock For Light',
        band: 'Bad Brains',
        format: 'LP',
        year: 1983,
        url: 'http://bandcamp.com/badbrains',
        label: 'PVC'
      });
      record.save((err, obj) => {
        chai.request(app)
          .get(`/api/records/${obj.id}`)
          .send(record)
          .end((error, res) => {
            should.not.exist(error);
            res.status.should.be.equal(200);
            res.type.should.equal('application/json');
            res.body.should.be.a('object');
            res.body.should.have.property('title');
            res.body.should.have.property('band');
            res.body.should.have.property('label');
            res.body.should.have.property('url');
            res.body.should.have.property('format');
            res.body.should.have.property('year');
            res.body.should.have.property('_id').eql(record.id);
            done();
          });
      });
    });
  });

  describe('/DELETE /records/:recordId', () => {
    it('it must delete that record', done => {
      const record = new Record({
        title: 'Wrong title',
        band: 'Wrong band',
        format: 'LP',
        year: 9999,
        url: 'http://bandcamp.com/malformedurl',
        label: 'Wrong label'
      });
      record.save((err, obj) => {
        chai.request(app)
          .delete(`/api/records/${obj.id}`)
          .end((error, res) => {
            should.not.exist(error);
            res.status.should.be.equal(200);
            res.type.should.equal('application/json');
            res.body.should.be.a('object');
            res.body.should.have.property('message').eql('Successfully deleted');
            res.body.should.have.property('_id').eql(obj.id);
            done();
          });
      });
    });
  });

  describe('/PUT /records/:recordId', () => {
    it('it must update the desired fields for correspondent record', done => {
      const record = new Record({
        title: 'Condensed Flesh',
        band: 'Void',
        format: '7"',
        year: 1992,
        url: 'http://bandcamp.com/void',
        label: 'Eye 95 Records'
      });
      record.save((err, obj) => {
        chai.request(app)
          .put(`/api/records/${obj.id}`)
          .send({ year: 1994, label: 'Lost & Found Records' })
          .end((error, res) => {
            should.not.exist(error);
            res.status.should.be.equal(200);
            res.type.should.equal('application/json');
            res.body.should.be.a('object');
            res.body.should.have.property('message').eql('Successfully updated');
            res.body.record.should.have.property('year').eql(1994);
            res.body.record.should.have.property('label').eql('Lost & Found Records');
            done();
          });
      });
    });
  });

  describe('/DELETE Complete table', () => {
    it('it must empty the records', done => {
      Record.remove({}).then((res, err) => {
        should.not.exist(err);
        res.result.should.be.a('object');
        res.result.ok.should.be.equal(1);
        res.result.n.should.be.above(0);
        chai.request(app)
          .get('/api/records')
          .end((error, records) => {
            should.not.exist(error);
            records.status.should.be.equal(200);
            records.type.should.equal('application/json');
            records.body.should.be.a('array');
            records.body.length.should.be.equal(0);
            done();
          });
      }).catch(done('Not emptied'));
    });
  });

  describe('App initial data', () => {
    it('it must add a record', done => {
      const record = new Record({
        title: 'Self Portrait',
        band: 'Loma Prieta',
        format: 'LP',
        year: 2015,
        url: 'http://bandcamp.com/lomaprieta',
        label: 'Deathwish'
      });
      chai.request(app)
        .post('/api/records/create')
        .send(record)
        .end((err, res) => {
          should.not.exist(err);
          res.status.should.be.equal(200);
          res.type.should.equal('application/json');
          res.body.should.be.a('object');
          res.body.should.have.property('message').eql('Successfully created');
          res.body.record.should.have.property('year').eql(2015);
          res.body.record.should.have.property('label').eql('Deathwish');
          res.body.record.should.have.property('format').eql('LP');
          res.body.record.should.have.property('url').eql('http://bandcamp.com/lomaprieta');
          res.body.record.should.have.property('band').eql('Loma Prieta');
          res.body.record.should.have.property('title').eql('Self Portrait');
          done();
        });
    });
    it('it must add another record', done => {
      const record = new Record({
        title: 'Jupiter',
        band: 'Cave In',
        format: 'LP',
        year: 2003,
        url: 'http://bandcamp.com/cavein',
        label: 'Hydrahead'
      });
      chai.request(app)
        .post('/api/records/create')
        .send(record)
        .end((err, res) => {
          should.not.exist(err);
          res.status.should.be.equal(200);
          res.type.should.equal('application/json');
          res.body.should.be.a('object');
          res.body.should.have.property('message').eql('Successfully created');
          res.body.record.should.have.property('year').eql(2003);
          res.body.record.should.have.property('label').eql('Hydrahead');
          res.body.record.should.have.property('format').eql('LP');
          res.body.record.should.have.property('url').eql('http://bandcamp.com/cavein');
          res.body.record.should.have.property('band').eql('Cave In');
          res.body.record.should.have.property('title').eql('Jupiter');
          done();
        });
    });
    it('it must add another record', done => {
      const record = new Record({
        title: 'Jane Doe',
        band: 'Converge',
        format: '2LP',
        year: 2001,
        url: 'http://bandcamp.com/converge',
        label: 'Equal Vision'
      });
      chai.request(app)
        .post('/api/records/create')
        .send(record)
        .end((err, res) => {
          should.not.exist(err);
          res.status.should.be.equal(200);
          res.type.should.equal('application/json');
          res.body.should.be.a('object');
          res.body.should.have.property('message').eql('Successfully created');
          res.body.record.should.have.property('year').eql(2001);
          res.body.record.should.have.property('label').eql('Equal Vision');
          res.body.record.should.have.property('format').eql('2LP');
          res.body.record.should.have.property('url').eql('http://bandcamp.com/converge');
          res.body.record.should.have.property('band').eql('Converge');
          res.body.record.should.have.property('title').eql('Jane Doe');
          done();
        });
    });
  });
});
