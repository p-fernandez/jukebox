import express from 'express';
import path from 'path';
import bodyParser from 'body-parser';
import PrettyError from 'pretty-error';
import mongoose from 'mongoose';
import index from './routes/index';
import records from './routes/record';
import config from './config/config';

const pretty = new PrettyError();
const app = express();
app.set('port', (process.env.PORT || 3030));

if (process.env.NODE_ENV === 'production') {
  app.use(express.static(path.resolve(__dirname, '..', 'build')));
}
console.log('Script ', path.resolve(__dirname, 'server.js'));

app.locals.title = 'Jukebox API';
app.use(bodyParser.urlencoded({ extended: true }));
app.use(bodyParser.json());
app.use('/api', index);
app.use('/api', records);

app.use(express.static(path.resolve(__dirname, '..', 'build')));

app.get('*', (req, res) => {
  const way = path.join(__dirname, 'build', 'index.html');
  console.log(way);
  res.sendFile(way);
});

const conn = `mongodb://${config.db.user}:${config.db.pwd}@${config.db.host0},${config.db.host1},${config.db.host2}/${config.db.name}?ssl=true&replicaSet=${config.db.replica}&authSource=${config.db.authSource}`;
const mongoOpt = {
  server: {
    socketOptions: {
      keepAlive: 1, connectTimeoutMS: 30000
    }
  },
  replset: {
    socketOptions: {
      keepAlive: 1, connectTimeoutMS: 30000
    }
  }
};
mongoose.Promise = global.Promise;
mongoose.connect(conn, mongoOpt, err => {
  if (err) {
    console.error('DB ERROR:', pretty.render(err));
  }
});

const db = mongoose.connection;
db.on('error', console.error.bind(console, 'MongoDB connection error:'));

app.listen(app.get('port'), () => {
  console.log(`Find the server at: ${config.api.full}`); // eslint-disable-line no-console
});

export default app;
