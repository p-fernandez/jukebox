import Record from '../models/record';

export default class RecordController {

  static create(req, res) {
    const data = new Record(req.body);
    data.save((err, record) => {
      if (err) {
        res.send(err);
      } else {
        res.json({ record, message: 'Successfully created' });
      }
    }).catch(err => {
      res.json({ message: err.message, error: err.name });
    });
  }

  static delete(req, res) {
    Record.findById(req.params.recordId).then((record, error) => {
      if (error || !record) {
        res.send({ message: 'Record not found', error });
      } else {
        const recordToDel = new Record();
        Object.assign(recordToDel, record).remove()
        .then(result => {
          res.json({ message: 'Successfully deleted', _id: result._id });
        })
        .catch(err => {
          res.json({ message: err.message, error: err.name });
        });
      }
    });
  }

  static get(req, res) {
    Record.findById(req.params.recordId).then((record, err) => {
      if (err) {
        res.send(err);
      }
      res.json(record);
    }).catch(err => {
      res.json({ message: err.message, error: err.name });
    });
  }

  static getAll(req, res) {
    Record.find().then((records, err) => {
      if (err) {
        res.send(err);
      }
      res.json(records);
    }).catch(err => {
      res.json({ message: err.message, error: err.name });
    });
  }

  static update(req, res) {
    Record.findById(req.params.recordId, (err, record) => {
      if (err) {
        res.send(err);
      }
      Object.assign(record, req.body).save((error, obj) => {
        if (error) {
          res.send(error);
        }
        res.json({ record: obj, message: 'Successfully updated' });
      });
    });
  }
}
