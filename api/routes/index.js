import express from 'express';

const index = express.Router();

index.use((req, res, next) => {
  next();
});

index.get('/', (req, res) => {
  res.json({ message: 'API endpoint!' });
});

export default index;
