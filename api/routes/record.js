import express from 'express';
import RecordController from '../controllers/record';

const records = express.Router();

records.use((req, res, next) => {
  next();
});

records.route('/records')
  .get((req, res) => {
    RecordController.getAll(req, res);
  });

records.route('/records/create')
  .post((req, res) => {
    RecordController.create(req, res);
  });

records.route('/records/:recordId')
  .get((req, res) => {
    RecordController.get(req, res);
  })
  .put((req, res) => {
    RecordController.update(req, res);
  })
  .delete((req, res) => {
    RecordController.delete(req, res);
  });

export default records;
