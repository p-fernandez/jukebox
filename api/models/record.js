import mongoose from 'mongoose';

const Schema = mongoose.Schema;
const RecordSchema = new Schema({
  title: String,
  band: String,
  year: Number,
  format: String,
  url: String,
  label: String
});

export default mongoose.model('Record', RecordSchema);
