import express from 'express';
import bodyParser from 'body-parser';
import path from 'path';
import PrettyError from 'pretty-error';
import mongoose from 'mongoose';
import index from './routes/index';
import records from './routes/record';
import config from './config/config';

const pretty = new PrettyError();
const app = express();
const apiPort = process.env.PORT || config.api.port;
const staticFiles = express.static(path.join(__dirname, '../build'));

app.locals.title = 'Jukebox API';
app.use(staticFiles);

app.use(bodyParser.urlencoded({ extended: true }));
app.use(bodyParser.json());
app.use('/api', index);
app.use('/api', records);

app.use('/*', staticFiles);

app.use((req, res, next) => {
  const err = new Error('Not found');
  err.status = 404;
  next(err);
});

app.use((err, req, res, next) => {
  console.error(err.stack);
  res.status(500).json({ message: 'Not available', error: err });
});

const conn = `mongodb://${config.db.user}:${config.db.pwd}@${config.db.host0},${config.db.host1},${config.db.host2}/${config.db.name}?ssl=true&replicaSet=${config.db.replica}&authSource=${config.db.authSource}`;
const mongoOpt = {
  server: {
    socketOptions: {
      keepAlive: 1, connectTimeoutMS: 30000
    }
  },
  replset: {
    socketOptions: {
      keepAlive: 1, connectTimeoutMS: 30000
    }
  }
};
mongoose.Promise = global.Promise;
mongoose.connect(conn, mongoOpt, err => {
  if (err) {
    console.error('DB ERROR:', pretty.render(err));
  }
});

const db = mongoose.connection;
db.on('error', console.error.bind(console, 'MongoDB connection error:'));

if (apiPort) {
  app.listen(apiPort, error => {
    if (error) {
      console.error(pretty.render(error));
      // setTimeout(() => app.close(), 3000);
    }
    console.info('\n==> API is running on port %s', apiPort);
    console.info('==> Send requests to %s', config.client.full);
  });
}

export default app;
