import React from 'react';
import ReactDOM from 'react-dom';
import { Provider } from 'react-redux';
import { syncHistoryWithStore } from 'react-router-redux';
import { Router, Route, IndexRoute, browserHistory } from 'react-router';
import store from './redux/store';
import { App, Jukebox, NotFound } from './containers';
import './index.css';

const history = syncHistoryWithStore(browserHistory, store);

const Root = () => (
  <Provider store={store}>
    <Router history={history}>
      <Route path="/" exact component={App} >
        <IndexRoute component={Jukebox} />
        <Route path="*" component={NotFound} status={404} />
      </Route>
    </Router>
  </Provider>
);

ReactDOM.render(
  <Root />,
  document.getElementById('root')
);
