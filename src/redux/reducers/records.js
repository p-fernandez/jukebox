import {
  CREATE_MODE,
  CREATE_PENDING,
  CREATE_FULFILLED,
  CREATE_REJECTED,
  EDIT_MODE,
  LOAD_PENDING,
  LOAD_FULFILLED,
  LOAD_REJECTED,
  MOVE_UP,
  MOVE_DOWN,
  REMOVE_PENDING,
  REMOVE_FULFILLED,
  REMOVE_REJECTED,
  UPDATE_PENDING,
  UPDATE_FULFILLED,
  UPDATE_REJECTED
 } from '../constants';

function swap(it, x, y) {
  const tmp = it.slice();
  const f = it[x];
  it[x] = it[y];
  it[y] = f;
  return it;
}

const initialState = {
  creating: false,
  isLoading: false,
  error: {}
};

const editMode = (state, id, status) => {
  const newData = state.data.map(el => {
    if (el._id === id) {
      return Object.assign({}, el, { mode: status });
    }
    return el;
  });
  return {
    ...state,
    data: newData
  };
};

function moveDown(state, id) {
  const pos = state.data.findIndex(el => el._id === id);
  if (pos < state.data.length - 1) {
    const newState = Object.assign({}, state);
    const swapped = swap(newState.data, pos, pos + 1);
    return {
      ...state,
      data: swapped,
      moved: id,
      moveDirection: 'down',
      oldPos: pos
    };
  }
  return {
    ...state,
    moveDirection: 'still'
  };
}

function moveUp(state, id) {
  const pos = state.data.findIndex(el => el._id === id);
  if (pos > 0) {
    const newState = Object.assign({}, state);
    const swapped = swap(newState.data, pos, pos - 1);
    return {
      ...state,
      data: swapped,
      moved: id,
      moveDirection: 'up',
      oldPos: pos
    };
  }
  return {
    ...state,
    moveDirection: 'still'
  };
}

const removeRecord = (state, res) => {
  const filtered = state.data.filter(el => el._id !== res._id);
  return {
    ...state,
    data: filtered,
    removing: null,
    removed: true
  };
};

const updateRecord = (state, res) => {
  const newState = Object.assign({}, state);
  newState.data.map(el => {
    if (el._id === res.record._id) {
      return Object.assign(el, res.record, { mode: false });
    }
    return el;
  });
  return {
    ...newState,
    updating: null,
    updated: true
  };
};

export default function records(state = initialState, action) {
  switch (action.type) {
    case CREATE_MODE:
      return {
        ...state,
        creating: !state.creating
      };
    case CREATE_PENDING:
      return {
        ...state,
        isCreating: true
      };
    case CREATE_FULFILLED:
      return {
        ...state,
        creating: false,
        isCreating: null,
        created: true,
        result: action.result,
        error: null
      };
    case CREATE_REJECTED:
      return {
        ...state,
        isCreating: null,
        created: false,
        error: action.error
      };
    case EDIT_MODE:
      return editMode(state, action.param.id, action.param.status);
    case LOAD_PENDING:
      return Object.assign(
        {},
        state,
        {
          isLoading: true,
          loaded: false,
        }
      );
    case LOAD_FULFILLED:
      return Object.assign(
        {},
        state,
        {
          error: null,
          isLoading: null,
          loaded: true,
          data: action.result
        }
      );
    case LOAD_REJECTED:
      return Object.assign(
        {},
        state,
        {
          isLoading: null,
          loaded: false,
          data: null,
          error: action.error
        }
      );
    case MOVE_UP:
      return moveUp(state, action.param);
    case MOVE_DOWN:
      return moveDown(state, action.param);
    case REMOVE_PENDING:
      return {
        ...state,
        removing: true,
        removed: false
      };
    case REMOVE_FULFILLED:
      return removeRecord(state, action.result);
    case REMOVE_REJECTED:
      return {
        ...state,
        removing: null,
        removed: false,
        error: action.error
      };
    case UPDATE_PENDING:
      return {
        ...state,
        updating: true,
        updated: false
      };
    case UPDATE_FULFILLED:
      return updateRecord(state, action.result);
    case UPDATE_REJECTED:
      return {
        ...state,
        updating: null,
        updated: false,
        error: action.error
      };
    default:
      return state;
  }
}
