import {
  CREATE_MODE,
  CREATE_PENDING,
  CREATE_FULFILLED,
  CREATE_REJECTED,
  EDIT_MODE,
  LOAD_PENDING,
  LOAD_FULFILLED,
  LOAD_REJECTED,
  MOVE_UP,
  MOVE_DOWN,
  REMOVE_PENDING,
  REMOVE_FULFILLED,
  REMOVE_REJECTED,
  UPDATE_PENDING,
  UPDATE_FULFILLED,
  UPDATE_REJECTED
} from '../constants';

export const load = () => ({
  types: [LOAD_PENDING, LOAD_FULFILLED, LOAD_REJECTED],
  promise: () => fetch('/api/records').then(response => response.json()).catch(error => error.json())
});

export const add = data => ({
  types: [CREATE_PENDING, CREATE_FULFILLED, CREATE_REJECTED],
  promise: () => fetch('/api/records/create', {
    method: 'POST',
    body: JSON.stringify(data),
    headers: new Headers({
      'Content-Type': 'application/json',
      Accept: 'application/json',
    })
  }).then(response => response.json())
  .catch(error => error.json())
});

export const create = () => ({
  type: CREATE_MODE
});

export const edit = (id, status) => ({
  type: EDIT_MODE,
  param: { id, status }
});

export const moveDown = id => ({
  param: id,
  type: MOVE_DOWN,
});

export const moveUp = id => ({
  param: id,
  type: MOVE_UP,
});

export const remove = id => ({
  types: [REMOVE_PENDING, REMOVE_FULFILLED, REMOVE_REJECTED],
  promise: () => fetch(`/api/records/${id}`, {
    method: 'DELETE',
    headers: new Headers({
      'Content-Type': 'application/json',
      Accept: 'application/json',
    })
  }).then(response => response.json())
  .catch(error => error.json())
});

export const update = values => ({
  types: [UPDATE_PENDING, UPDATE_FULFILLED, UPDATE_REJECTED],
  promise: () => fetch(`/api/records/${values._id}`, {
    method: 'PUT',
    body: JSON.stringify(values),
    headers: new Headers({
      'Content-Type': 'application/json',
      Accept: 'application/json',
    })
  }).then(response => response.json())
  .catch(error => error.json())
});
