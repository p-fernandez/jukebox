import { combineReducers } from 'redux';
import { reducer as formReducer } from 'redux-form';
import { routerReducer } from 'react-router-redux';

import records from './reducers/records';

const reducer = combineReducers({
  routing: routerReducer,
  form: formReducer,
  records
});

export default reducer;
