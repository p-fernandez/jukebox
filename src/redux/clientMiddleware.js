const clientMiddleware = () => {
  return ({ dispatch, getState }) => {
    return next => action => {
      if (typeof action === 'function') {
        return action(dispatch, getState);
      }
      const { promise, types, ...rest } = action;
      if (!promise) {
        return next(action);
      }

      const [PENDING, FULFILLED, REJECTED] = types;
      next({ ...rest, type: PENDING });

      const actionPromise = promise();
      actionPromise.then(
        result => next({ ...rest, result, type: FULFILLED }),
        error => next({ ...rest, error, type: REJECTED })
      ).catch(error => {
        console.error('MIDDLEWARE ERROR:', error);
        next({ ...rest, error, type: REJECTED });
      });

      return actionPromise;
    };
  };
};

export default clientMiddleware;
