export const CREATE_MODE = 'records/CREATE_MODE';
export const CREATE_PENDING = 'records/CREATE_PENDING';
export const CREATE_FULFILLED = 'records/CREATE_FULFILLED';
export const CREATE_REJECTED = 'records/CREATE_REJECTED';
export const EDIT_MODE = 'records/EDIT_MODE';
export const LOAD_PENDING = 'records/LOAD_PENDING';
export const LOAD_FULFILLED = 'records/LOAD_FULFILLED';
export const LOAD_REJECTED = 'records/LOAD_REJECTED';
export const MOVE_UP = 'records/MOVE_UP';
export const MOVE_DOWN = 'records/MOVE_DOWN';
export const REMOVE_PENDING = 'records/REMOVE_PENDING';
export const REMOVE_FULFILLED = 'records/REMOVE_FULFILLED';
export const REMOVE_REJECTED = 'records/REMOVE_REJECTED';
export const UPDATE_PENDING = 'records/UPDATE_PENDING';
export const UPDATE_FULFILLED = 'records/UPDATE_FULFILLED';
export const UPDATE_REJECTED = 'records/UPDATE_REJECTED';

