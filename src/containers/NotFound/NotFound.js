import React from 'react';
import { Loading } from '../../components';
import './NotFound.css';

const NotFound = () => ({
  render() {
    return (
      <div className="NotFound">
        <div className="LNotFound">4</div>
        <Loading />
        <div className="RNotFound">4</div>
      </div>
    );
  }
});

export default NotFound;
