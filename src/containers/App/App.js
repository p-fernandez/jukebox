import React from 'react';
import './App.css';

const App = () => ({
  render() {
    return (
      <div className="App">
        <div className="App-header">
          <h1>THE JUKEBOX</h1>
        </div>
        <div className="App-intro">
          { this.props.children }
        </div>
      </div>
    );
  }
});

App.propTypes = {
  children: React.PropTypes.element
};

export default App;
