import React from 'react';
import { connect } from 'react-redux';
import { Loading, Record, AddRecord } from '../../components';
import * as actionRecords from '../../redux/actions';
import './Jukebox.css';

const mapStateToProps = store => {
  const mods = {
    dispatch: store.dispatch,
    records: store.records.data,
    loaded: store.records.loaded,
    isLoading: store.records.isLoading,
    removing: store.records.removing
  };
  return Object.assign({}, store, mods);
};

const Jukebox = () => ({
  componentDidMount() {
    if (!this.props.records) {
      this.props.load();
    }
  },
  render() {
    const {
      error,
      isLoading,
      records,
      removing
    } = this.props;
    if (isLoading || removing) {
      return (
        <div className="Jukebox">
          <div className="Jukebox-loading">
            <Loading />
          </div>
        </div>
      );
    }
    return (
      <div className="Jukebox">
        <div className="Jukebox-header">
          <div className="j1" />
          <div className="j2" />
          <div className="j3" />
          <div className="j4" />
        </div>
        <div className="Jukebox-body">
          { records &&
          records.map(record =>
            <Record
              key={record._id}
              {...record}
            />)}
          <AddRecord />
        </div>
      </div>
    );
  }
});

export default connect(mapStateToProps, actionRecords)(Jukebox);
