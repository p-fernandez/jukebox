import React from 'react';
import './Loading.css';

const Loading = () => ({
  render() {
    return (
      <div className="LoaderContainer">
        <div className="Loader" />
      </div>
    );
  }
});

export default Loading;
