import React from 'react';
import { connect } from 'react-redux';
import RecordForm from './RecordForm';
import { add, create, load } from '../../redux/actions';
import './Record.css';
import './AddRecord.css';
import './RecordForm.css';

const mapStateToProps = store => ({
  creating: store.records.creating
});

const mapDispatchToProps = dispatch => ({
  addRecord: data => dispatch(add(data)).then(dispatch(load())).catch(er => console.error(er)),
  toggleMode: () => dispatch(create())
});

const AddRecord = () => ({
  render() {
    const { creating, addRecord, toggleMode } = this.props;
    if (creating) {
      return (
        <div className="Record">
          <RecordForm
            form="createRecord"
            onSubmit={addRecord}
            titlePrimary="Create"
            onSecondary={toggleMode}
            titleSecondary="Cancel"
          />
          <div className="Edit" />
        </div>
      );
    }
    return (
      <div className="Record">
        <button className="New" onClick={toggleMode}>Add</button>
      </div>
    );
  }
});

AddRecord.propTypes = {
  addRecord: React.PropTypes.func.isRequired,
  toggleMode: React.PropTypes.func.isRequired,
  creating: React.PropTypes.bool.isRequired
};

AddRecord.default = {
  creating: false
};

export default connect(mapStateToProps, mapDispatchToProps)(AddRecord);
