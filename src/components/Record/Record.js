import React from 'react';
import { connect } from 'react-redux';
import Toggle from 'react-toggle';
import RecordInfo from './RecordInfo';
import RecordForm from './RecordForm';
import { edit, load, moveDown, moveUp, remove, update } from '../../redux/actions';
import './Record.css';

const mapStateToProps = (store, ownProps) => {
  const record = store.records.data.find(el => {
    if (el._id === ownProps._id) {
      return el;
    }
    return false;
  });
  return {
    record
  };
};

const mapDispatchToProps = (dispatch, ownProps) => ({
  edit: (_id, status) => dispatch(edit(_id, status)),
  doMoveDown: () => dispatch(moveDown(ownProps._id)),
  doMoveUp: () => dispatch(moveUp(ownProps._id)),
  toggleEditMode: (event, _id) => dispatch(edit(_id, event.target.checked)),
  removeRecord: _id => dispatch(remove(_id)).then(load()).catch(er => console.log(er)),
  updateRecord: data => dispatch(update(data)).then(load()).catch(er => console.log(er))
});

const Record = () => ({
  render() {
    const {
      _id,
      band,
      format,
      label,
      mode,
      title,
      url,
      year,
      record,
      doMoveDown,
      doMoveUp,
      toggleEditMode,
      removeRecord,
      updateRecord
    } = this.props;
    const initialValues = { _id, band, format, label, mode, title, url, year };
    if (mode) {
      return (
        <div className="EditRecord">
          <div className="Up"><button onClick={doMoveUp}><span /></button></div>
          <RecordForm
            className="EditWrapper"
            form={_id}
            initialValues={initialValues}
            onSubmit={updateRecord}
            onSecondary={removeRecord}
            secondaryParam={_id}
            titlePrimary="Update"
            titleSecondary="Delete"
          />
          <div className="Down"><button onClick={doMoveDown}><span /></button></div>
          <div className="Edit">
            <Toggle
              id={`edit-${_id}`}
              checked={mode}
              onChange={e => toggleEditMode(e, _id)}
            />
            <label htmlFor={`edit-${_id}`}>Edit</label>
          </div>
        </div>
      );
    }
    return (
      <div className="Record">
        <div className="Up"><button onClick={doMoveUp}><span /></button></div>
        <RecordInfo {...record} />
        <div className="Down"><button onClick={doMoveDown}><span /></button></div>
        <div className="Edit">
          <Toggle
            id={`edit-${_id}`}
            checked={mode}
            onChange={e => toggleEditMode(e, _id)}
          />
          <label htmlFor={`edit-${_id}`}>Edit</label>
        </div>
      </div>
    );
  }
});

Record.propTypes = {
  _id: React.PropTypes.string.isRequired,
  band: React.PropTypes.string.isRequired,
  doMoveDown: React.PropTypes.func.isRequired,
  doMoveUp: React.PropTypes.func.isRequired,
  edit: React.PropTypes.func.isRequired,
  format: React.PropTypes.string.isRequired,
  label: React.PropTypes.string.isRequired,
  mode: React.PropTypes.bool,
  record: React.PropTypes.shape({
    _id: React.PropTypes.string.isRequired,
    band: React.PropTypes.string.isRequired,
    format: React.PropTypes.string.isRequired,
    label: React.PropTypes.string.isRequired,
    title: React.PropTypes.string.isRequired,
    url: React.PropTypes.string.isRequired,
    year: React.PropTypes.number.isRequired
  }).isRequired,
  removeRecord: React.PropTypes.func.isRequired,
  title: React.PropTypes.string.isRequired,
  toggleEditMode: React.PropTypes.func.isRequired,
  updateRecord: React.PropTypes.func.isRequired,
  url: React.PropTypes.string.isRequired,
  year: React.PropTypes.number.isRequired
};

Record.defaultProps = {
  mode: false
};

export default connect(mapStateToProps, mapDispatchToProps)(Record);
