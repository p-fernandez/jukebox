import React from 'react';
import './Record.css';

const RecordInfo = () => ({
  render() {
    const { band, format, title, year, label, url } = this.props;
    return (
      <div className="Info">
        <div className="Band multiple-desktop">{band}</div>
        <div className="Format multiple-desktop">{format}</div>
        <div className="Title multiple-desktop">{title}</div>
        <div className="Year multiple-desktop">{`(${year})`}</div>
        <div className="Label multiple-desktop">{label}</div>
        <div className="Url multiple-desktop">{url}</div>
      </div>
    );
  }
});

export default RecordInfo;
