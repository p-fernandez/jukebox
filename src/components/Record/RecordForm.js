import React from 'react';
import { connect } from 'react-redux';
import { Field, reduxForm } from 'redux-form';
import './Record.css';
import './RecordForm.css';

const mapStateToProps = store => ({
  dispatch: store.dispatch,
  stateForm: store.form
});

const mapDispatchToProps = () => ({
  submitHandler: (e, action, stateForm) => {
    e.preventDefault();
    const formName = Object.keys(stateForm)[0];
    action(stateForm[formName].values);
  },
  secondaryHandler: (e, action, param) => {
    e.preventDefault();
    action(param);
  }
});

const RecordForm = () => ({

  render() {
    const {
      className,
      stateForm,
      onSecondary,
      onSubmit,
      secondaryHandler,
      secondaryParam,
      submitHandler,
      titlePrimary,
      titleSecondary
    } = this.props;
    return (
      <form className={className} onSubmit={e => submitHandler(e, onSubmit, stateForm)}>
        <div className="Info">
          <div className="Band multiple-desktop">
            <Field name="band" type="text" component="input" />
            <label htmlFor="band">Band</label>
          </div>
          <div className="Format multiple-desktop">
            <Field name="format" type="text" component="input" />
            <label htmlFor="format">Format</label>
          </div>
          <div className="Title multiple-desktop">
            <Field name="title" type="text" component="input" />
            <label htmlFor="title">Title</label>
          </div>
          <div className="Year multiple-desktop">
            <Field name="year" type="text" component="input" />
            <label htmlFor="year">Year</label>
          </div>
          <div className="Label multiple-desktop">
            <Field name="label" type="text" component="input" />
            <label htmlFor="label">Label</label>
          </div>
          <div className="Url multiple-desktop">
            <Field name="url" type="text" component="input" />
            <label htmlFor="url">Url</label>
          </div>
        </div>
        <div className="Actions">
          <button type="submit">{titlePrimary}</button>
          <button
            onClick={e => secondaryHandler(e, onSecondary, secondaryParam)}
          >
            {titleSecondary}
          </button>
        </div>
      </form>
    );
  }
});

RecordForm.propTypes = {
  onSecondary: React.PropTypes.func.isRequired,
  onSubmit: React.PropTypes.func.isRequired,
  secondaryParam: React.PropTypes.string,
  titlePrimary: React.PropTypes.string.isRequired,
  titleSecondary: React.PropTypes.string.isRequired
};

const theRecordForm = reduxForm({})(RecordForm);

export default connect(mapStateToProps, mapDispatchToProps)(theRecordForm);
