export { default as Loading } from './Loading/Loading';
export { default as AddRecord } from './Record/AddRecord';
export { default as Record } from './Record/Record';
export { default as RecordForm } from './Record/RecordForm';
