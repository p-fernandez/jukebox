# [Jukebox](https://gentle-depths-77755.herokuapp.com/)

## Overview

SPA sample application based in Node.js, React, Redux and Mongoose (MongoDB tied).

Point of start was Create React App boilerplate: [Create React App](https://github.com/facebookincubator/create-react-app)


## Getting started 

Prerrequisites

1. A config.js file with the db configuration is needed for Node.js to run everything. Some added variables not fully needed
for configuring the server.
2. The structure of said config file is like this:

```javascript
const config = { client: {}, api: {}, db: {} };

config.client.url = URL 
config.client.port = 3000;
config.client.protocol = 'http';
config.client.full = `${config.client.protocol}://${config.client.url}:${config.client.port}`;
config.api.url = URL 
config.api.port = 3030;
config.api.protocol = 'http';
config.api.full = `${config.api.protocol}://${config.api.url}:${config.api.port}`;

config.db.user = USER 
config.db.pwd = PASSWORD
config.db.host0 = MONGODB ATLAS HOST 0 
config.db.host1 = MONGODB ATLAS HOST 1 
config.db.host2 = MONGODB ATLAS HOST 2 
config.db.name = DATABASE 
config.db.replica = REPLICA 
config.db.authSource = ADMIN 

export default config;
```
3. In package.json the line 
```javascript "proxy": "http://localhost:3030"``` 
helps the client side to find the API endpoint. 
Needed and useful in order to be able to use ```fetch``` for handling API promises.
Remember to update it with the production URL when staging up.


## Process

1. Run Create React App
```bash
npm init
npm install
```

2. Set up Express as server framework base combined with React as view layer.

3. Set up Mongoose to connect the App to a Mongo DB hosted in a free account at MongoDB Atlas.

4. Developed simple CRUD controller API and Node.js routing to connect server to Mongo DB with Mongoose ORM.

5. Tested API with Jest and Chai.

6. Installed React Router 3 and configured server to delegate routing to React Router, even if for this SPA few routing is needed.

7. Set up Redux 3 for managing App state. Integrated with React and React Router, besides implementing thunk and a custom API client
middleware for managing actions with ES6 promises.
Custom API client middleware is provided by @erikras [React Redux Universal Hot Example boilerplate](https://github.com/erikras/react-redux-universal-hot-example)

8. Integrated Redux Form in order to achieve the CRUD client side functionality.

9. Some fancy CSS3.

## Patterns

1. Composite (Redux middlewares and React class / function coupling with Redux action creators)
2. Decorator (React-Redux coupling, Redux Form)
3. Facade (API)
4. Proxy (React objects)
5. Iterator (Custom API client middleware)
6. Observer (React components connected to Redux and re-rendering after state changes)
7. Template method (Applied to my Redux form implementation for applying dynamic behaviour to form buttons depending on parent object)

Unsure if Flyweight and Memento are patterns implemented in Redux store.

## Complications founds

1. First time ever using a NoSQL database (Mongo DB) and any related tool to said technology.

2. Also first time ever in building a React Redux App from scratch. I tend to start through boilerplates and modify them as needed.
Due the deadline and the requirement to be able to deploy the project in an external server, I relied in Create React App to avoid 
the complexity of setting a fully functional Webpack project in a timely manner.

3. Probably because the last step, all React re-render events are not fully working after a Redux store state change. Spent much time
debugging looking for the issue but haven't found the reason of some lousy and sketchy behaviour. Container and components are 
properly connected as the React workgroup recommends. Redux logger tracks and fire all the events occurring and store is updated
properly, but some re-renders are not fired.
Also checked the prevention of state mutability and no clue found about it. Seems to work flawlesly and the shallow comparing of
properties seem to work as expected.
Disguised some of the lousy behaviour by chaining the dispatching of redux actions with a fancy loader.

4. Previous step stopped me to accomplish my original idea (a catalog of records with API consuming from any music service as
Bandcamp or Soundcloud, for continuous music playlist playing; or to similarity behaviour with Youtube videos fetched in real time
depending on content) and leaving a simple client side CRUD App.
The side up and down buttons were one of the functionalities that wanted to implement for playlist reordering.

## Features missing

1. Redux form validation

2. "Disguise" of Redux functionality for up / down buttons with loader.